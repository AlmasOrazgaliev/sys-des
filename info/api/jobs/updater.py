from apscheduler.schedulers.background import BackgroundScheduler
from .jobs import task3


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(task3, 'interval', days=1)
    print('SCHEDULER STARTED')
    scheduler.start()
