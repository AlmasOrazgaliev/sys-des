import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from pymongo import MongoClient


def task3():
    absent_legal_address = 'https://kgd.gov.kz/mobile_api/services/taxpayers_unreliable_exportexcel/WRONG_ADDRESS/KZ_ALL/fileName/list_WRONG_ADDRESS_KZ_ALL.xlsx'
    declared_bankrupt = 'https://kgd.gov.kz/mobile_api/services/taxpayers_unreliable_exportexcel/BANKRUPT/KZ_ALL/fileName/list_BANKRUPT_KZ_ALL.xlsx'
    registration_invalid = 'https://kgd.gov.kz/mobile_api/services/taxpayers_unreliable_exportexcel/INVALID_REGISTRATION/KZ_ALL/fileName/list_INVALID_REGISTRATION_KZ_ALL.xlsx'
    absent_legal_address_data_types = {
        '№': int,
        'ИИН/БИН': str,
        'РНН': str,
        'Наименование налогоплательщика': str,
        'ФИО налогоплательщика': str,
        'ФИО руководителя': str,
        'ИИН руководителя': str,
        'РНН руководителя': str,
        'Номер акта обследования': str,
        'Дата акта обследования': str
    }
    absent_legal_address_names = ['№', 'ИИН/БИН', 'РНН', 'Наименование налогоплательщика', 'ФИО налогоплательщика',
                                  'ФИО руководителя',
                                  'ИИН руководителя', 'РНН руководителя', 'Номер акта обследования',
                                  'Дата акта обследования']

    db_write(absent_legal_address, 'absent_legal_address', absent_legal_address_data_types, absent_legal_address_names)
    print('absent_legal_address done')


    other_data_types = {
        '№': int,
        'ИИН/БИН': str,
        'РНН': str,
        'Наименование налогоплательщика': str,
        'ФИО налогоплательщика': str,
        'ФИО руководителя': str,
        'ИИН руководителя': str,
        'РНН руководителя': str,
        'Номер решения суда': str,
        'Дата решения суда': str
    }
    other_names = ['№', 'ИИН/БИН', 'РНН', 'Наименование налогоплательщика', 'ФИО налогоплательщика', 'ФИО руководителя',
                   'ИИН руководителя', 'РНН руководителя', 'Номер решения суда', 'Дата решения суда']

    db_write(declared_bankrupt, 'declared_bankrupt', other_data_types, other_names)
    print('declared_bankrupt done')

    db_write(registration_invalid, 'registration_invalid', other_data_types, other_names)
    print('registration_invalid done')



def db_write(url, name, data_types, names):
    df = pd.read_excel(
        url, skiprows=2,
        names=names,
        dtype=data_types)

    db_url = 'postgresql://postgres:alma45884@localhost:5432/postgres'
    engine = create_engine(db_url)
    Session = sessionmaker(bind=engine)
    session = Session()
    df.to_sql(name, engine, if_exists='replace', index=False)

    client = MongoClient('mongodb://localhost:27017')
    db = client['test']
    collection = db[name]
    collection.drop()
    collection.insert_many(df.to_dict('records'))
