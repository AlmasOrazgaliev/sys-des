from django.urls import path
from .views import *

urlpatterns = [
    path('task1', task1),
    path('task2_1/', Task2_1.as_view()),
    path('task2_2/', Task2_2.as_view()),
    path('task2_3/<str:number>/<str:lang>/', Task2_3.as_view()),
    path('task2_4/', Task2_4.as_view())

]
