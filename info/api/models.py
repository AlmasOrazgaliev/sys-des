from django.db import models


# Create your models here.
class Bin(models.Model):
    bin = models.CharField(max_length=100)
    lang = models.CharField(max_length=10)
    response = models.JSONField()