import io
import random
import requests
from bs4 import BeautifulSoup as bs
from django.core.exceptions import BadRequest
from django.http import JsonResponse
from .models import Bin
from rest_framework.views import APIView
from rest_framework.response import Response
import json
import zipfile
from django.http import HttpRequest
import pandas
from urllib.request import urlopen
# Create your views here.
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'
}


def get_random_proxy():
    url = "https://free-proxy-list.net/"
    soup = bs(requests.get(url).content, "html.parser")
    proxies = []
    for row in soup.find("table", attrs={"class": "table table-striped table-bordered"}).find_all("tr")[1:]:
        tds = row.find_all("td")[:100]
        try:
            ip = tds[0].text.strip()
            port = tds[1].text.strip()

            host = f"{ip}:{port}"
            proxies.append(host)
        except IndexError:
            continue
    return random.choice(proxies)


def task1(request):
    bin = request.GET.get('bin')
    if bin is None:
        raise BadRequest('Required String parameter "bin" is not present')
    lang = request.GET.get('lang')
    if lang is None:
        raise BadRequest('Required String parameter "lang" is not present')

    try:
        obj = Bin.objects.get(bin=bin, lang=lang)
        return JsonResponse(obj.response, safe=False)
    except Exception as e:
        pass

    model = Bin(bin=bin, lang=lang)

    bin = 'bin=' + bin
    lang = '&lang=' + lang
    # print("https://old.stat.gov.kz/api/juridical/counter/api/?" + bin + lang)
    for i in range(10):
        proxy = get_random_proxy()
        try:
            with requests.session() as session:
                res = session.get("https://old.stat.gov.kz/api/juridical/counter/api/?" + bin + lang,
                                  headers=headers, proxies={"http": "http://" + proxy}, timeout=1.5)
                print(proxy)
                model.response = res.json()
                model.save()
                return JsonResponse(res.json(), safe=False)
            # print(s.get("https://2ip.ru", timeout=1.5).text)
        except Exception as e:
            print(e)
            continue


class Task2_1(APIView):
    def get(self, request):
        response = requests.get('https://old.stat.gov.kz/api/rcut/ru')
        return Response(response.json())


class Task2_2(APIView):
    def post(self, request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        for i in range(10):
            proxy = get_random_proxy()
            print(proxy)
            try:
                res = requests.post('https://old.stat.gov.kz/api/sbr/request/?api', json=body,
                                    headers=headers, proxies={"http": "http://" + proxy}, timeout=1.5)
                return Response(res.json())
            except Exception as e:
                print(e)
                continue


class Task2_3(APIView):
    def get(self, request, number, lang):
        response = requests.get('https://stat.gov.kz/api/sbr/requestResult/' + number + '/' + lang)
        return Response(response.json())


def download_zip_file(url, output_path):
    try:
        # Send an HTTP request to the URL
        response = requests.get(url, stream=True)
        response.raise_for_status()  # Check for any errors in the request

        # Open a local file for writing
        with open(output_path, 'wb') as file:
            # Iterate over the content in chunks and write to the local file
            for chunk in response.iter_content(chunk_size=8192):
                if chunk:
                    file.write(chunk)

        return output_path  # Return the path to the downloaded zip file

    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")
        return None


def stream_zip_file(url):
        # Stream the zip file content from the URL
        response = requests.get(url, stream=True)
        for chunk in response.iter_content(chunk_size=8192):
            if chunk:
                yield chunk
class Task2_4(APIView):
    def get(self, request):
        headers = {
            'Content-Disposition': 'attachment; filename="downloaded_file.zip"',
            'Content-Type': 'application/zip'
        }

        bucket = request.GET.get('bucket')
        if bucket is None:
            raise BadRequest('Required String parameter "bucket" is not present')
        guid = request.GET.get('guid')
        if guid is None:
            raise BadRequest('Required String parameter "guid" is not present')

        bucket = 'bucket=' + bucket
        guid = '&guid=' + guid

        url = "https://stat.gov.kz/api/sbr/download?"+bucket+guid
        #print(url)
        # Stream the zip file content to the client
        return Response(stream_zip_file(url), headers=headers)
