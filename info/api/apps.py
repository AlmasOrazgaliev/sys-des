from django.apps import AppConfig
from .jobs import updater


class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api'
    def ready(self):
        updater.start()
