# Generated by Django 4.2.5 on 2023-09-19 11:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bin',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bin', models.CharField(max_length=100)),
                ('lang', models.CharField(max_length=10)),
                ('response', models.JSONField()),
            ],
        ),
    ]
